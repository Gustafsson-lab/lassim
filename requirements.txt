Cython>=0.25.2
matplotlib>=2.0.2
nose>=1.3.7
numpy>=1.12.1
pandas>=0.20.2
psutil>=5.2.1
scipy>=0.19.1
sortedcontainers>=1.5.7