from collections import namedtuple
from unittest import TestCase
from unittest.mock import create_autospec

import numpy as np
from lassim.lassim_context import LassimContext
from lassim.problems.core_problem import CoreProblemFactory, CoreProblem
from lassim.solutions.lassim_solution import CoreSolution
from nose.tools import assert_dict_equal, assert_false, assert_true, nottest, \
    assert_not_equal
from numpy.testing import assert_array_equal
from sortedcontainers import SortedDict, SortedSet, SortedList

from customs.core_functions import generate_reactions_vector, \
    remove_lowest_reaction, default_bounds, iter_function, \
    remove_under_threshold_reactions

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "1.1.0"


def fake_ode_function(*args):
    return np.reshape(np.linspace(0, 100, num=100), (10, 10))


class TestCoreFunctions(TestCase):
    def setUp(self):
        self.fake_network = SortedDict({
            "IRF4": SortedSet(["NFATC3"]),
            "MAF": SortedSet(["IRF4", "NFATC3", "STAT3"]),
            "NFATC3": SortedSet(),
            "STAT3": SortedSet(["IRF4"])
        })
        # this fake reactions are the same that would have been obtained from
        # creating a CoreSystem object from the fake network
        self.fake_reactions = SortedDict({
            0: SortedSet([2]),
            1: SortedSet([0, 2, 3]),
            2: SortedSet([0, 1, 2, 3]),
            3: SortedSet([0])
        })

        data = np.reshape(np.linspace(0, 100, num=100), (10, 10))
        sigma = np.linspace(0, 1, num=10)
        times = np.linspace(0, 100, num=10)
        y0 = np.linspace(0, 100, num=10)

        self.factory = CoreProblemFactory.new_instance(
            (data, sigma, times), y0, fake_ode_function
        )
        # 17 = 4lambdas + 4vmax + 9reactions
        self.decision_vector = np.linspace(0, 10, num=17)
        self.fitness = np.array(0.99, ndmin=1)
        # too lazy to create an entire CoreProblem
        self.FakeProblem = namedtuple("FakeProblem",
                                      ["vector_map", "vector_map_mask", "y0"])

    def test_DefaultBounds(self):
        expected = (np.array([0.0, 0.0, 0.0, 0.0, -20.0, -20.0]),
                    np.array([20.0, 20.0, 20.0, 20.0, 20.0, 20.0]))
        actual = default_bounds(2, 2)
        assert_array_equal(expected[0], actual[0],
                           "Expected lower bounds\n{}\nbut actual {}".format(
                               expected[0], actual[0]))
        assert_array_equal(expected[1], actual[1],
                           "Expected upper bounds\n{}\nbut actual {}".format(
                               expected[0], actual[0]))

    def test_GenerateReactionsVector(self):
        exp_reactions = np.array([0, 0, np.inf, 0,
                                  np.inf, 0, np.inf, np.inf,
                                  np.inf, np.inf, np.inf, np.inf,
                                  np.inf, 0, 0, 0], dtype=np.float32)
        exp_mask = np.array([False, False, True, False,
                             True, False, True, True,
                             True, True, True, True,
                             True, False, False, False])
        act_reactions, act_mask = generate_reactions_vector(self.fake_reactions)
        assert_array_equal(exp_reactions, act_reactions,
                           "Expected\n{}\nbut actual\n{}".format(
                               exp_reactions, act_reactions
                           ))
        assert_array_equal(exp_mask, act_mask,
                           "Expected\n{}\nbut actual\n{}".format(
                               exp_mask, act_mask
                           ))

    def test_RemoveLowestReaction(self):
        fake_result = np.array([0, 0, 0, 0,
                                0, 0, 0, 0,
                                -1, 2, 3, -4, 1, 0.5, 6, 0.5, -11])
        exp_result = np.array([0, 0, 0, 0,
                               0, 0, 0, 0,
                               -1, 2, 3, -4, 1, 6, 0.5, -11])
        exp_reactions = SortedDict({
            0: SortedSet([2]),
            1: SortedSet([0, 2, 3]),
            2: SortedSet([0, 2, 3]),
            3: SortedSet([0])
        })
        act_result, act_reactions, removed = remove_lowest_reaction(
            fake_result, self.fake_reactions
        )
        assert_array_equal(exp_result, act_result,
                           "Expected\n{}\nbut actual\n{}".format(
                               exp_result, act_result
                           ))
        assert_dict_equal(exp_reactions, act_reactions,
                          "Expected\n{}\nbut actual\n{}".format(
                              exp_reactions, act_reactions
                          ))
        assert_not_equal(self.fake_reactions, act_reactions)

    def test_RemoveReactionsUnderThreshold(self):
        threshold = 0.01  # 1% of max value
        fake_result = np.array([0, 0, 0, 0,
                                0, 0, 0, 0,
                                -1, 2, 3, -4, 1, 0.1, 6, -11, -0.005])
        expected_vector = np.array([0, 0, 0, 0,
                                    0, 0, 0, 0,
                                    -1, 2, 3, -4, 1, 6, -11], dtype=np.float)
        expected_reactions = SortedDict({
            0: SortedSet([2]),
            1: SortedSet([0, 2, 3]),
            2: SortedSet([0, 2, 3]),
            3: SortedSet([])
        })

        act_result, act_reactions, removed = remove_under_threshold_reactions(
            fake_result, self.fake_reactions, threshold
        )
        assert_array_equal(expected_vector, act_result,
                           "Expected\n{}\nbut actual\n{}".format(
                               expected_vector, act_result
                           ))
        assert_dict_equal(expected_reactions, act_reactions,
                          "Expected\n{}\nbut actual\n{}".format(
                              expected_reactions, act_reactions
                          ))

    # the mask used in the test_IterationFunction* doesn't really make sense.
    # What is important here, is that the function returns true or false
    # based on what is in the mask
    def test_IterationFunctionWithMaskAllFalse(self):
        mock_core = create_autospec(CoreProblem, instance=True)
        mock_core.vector_maps = (
            np.linspace(0, 10, 9), np.array([False for _ in range(0, 9)])
        )
        mock_core.y0 = np.linspace(0, 10)
        mock_context = create_autospec(LassimContext, instance=True)

        solution = CoreSolution(
            self.decision_vector, self.fitness,
            self.fake_reactions, mock_core
        )
        new_problem, new_reactions, iteration = iter_function(None)(
            self.factory, mock_context, SortedList([solution]), solution
        )
        assert_false(iteration, "An iteration wasn't expected.")

    # not able to understand why this test fails
    @nottest
    def test_IterationFunctionWithMaskOneTrue(self):
        mask = [False for _ in range(0, 8)]
        mask.append(True)
        solution = CoreSolution(
            self.decision_vector, self.fitness,
            self.fake_reactions,
            self.FakeProblem(np.linspace(0, 10, 9), np.array(mask))
        )
        new_problem, new_reactions, iteration = iter_function(
            self.factory, None, SortedList([solution])
        )
        assert_true(iteration, "An iteration was expected.")

    @nottest
    def test_IterationFunctionWithMaskUnlTrue(self):
        mask = [False for _ in range(0, 7)]
        mask.append(True)
        mask.append(True)
        solution = CoreSolution(
            self.decision_vector, self.fitness,
            self.fake_reactions,
            self.FakeProblem(np.linspace(0, 10, 9), np.array(mask))
        )
        for i in range(0, 9):
            new_problem, new_reactions, iteration = iter_function(
                self.factory, None, SortedList([solution])
            )
            assert_true(iteration, "Expected iteration at try {}".format(i))
            solution = CoreSolution(
                self.decision_vector, self.fitness, new_reactions, new_problem
            )
        new_problem, new_reactions, iteration = iter_function(
            self.factory, None, SortedList([solution])
        )
        assert_false(iteration, "No iteration expected at the end.")
