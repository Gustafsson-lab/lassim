INSTALLATION GUIDE
==================

The following installation instructions will detail how to install the necessary dependencies of the **LASSIM** toolbox 
in an Linux/Mac environment. 

**[!]** The Windows OS is not supported yet.

Anaconda installation
---------------------
The preferred Python3 platform is [Anaconda](https://anaconda.org/), a Python based platform containing most of the packages 
required for scientific computing. However, all the packages installed using Anaconda, can also be installed using the
classical Python3 environment.

Download [Anaconda3-4.x.y-Linux-x86_64.sh](https://repo.continuum.io/archive/index.html) and install it using the following command:
```bash
bash Anaconda3-4.x.y-Linux-x86_64.sh
source ~/.bashrc
```
Once the installation is completed, be sure that the Anaconda path is saved in your shell profile, usually inside
`~/.bashrc`, by checking that these lines are inside the file, or add them if they are missing
```
# added by Anaconda3 4.x.y installer
export PATH="<home>/anaconda3/bin:$PATH"
```
Before creating a virtual environment, it is necessary to install the following packages in the root environment
```bash
conda install libgcc
conda config --add channels conda-forge
conda install pygmo
```
Then, a virtual environment specific only for **LASSIM** can be created, running the following commands
```bash
conda create -n lassim --clone root
source activate lassim
```
From this point on `lassim` is the name of the virtual environment it is going to be used, but it is not mandatory to 
create one and/or use it.

**[Mac OS users]** There are few differences for Mac OS users to keep in mind:
- The installer to download for Anaconda is [Anaconda3-4.x.y-MacOSX-x86_64.pkg](https://repo.continuum.io/archive/index.html)
- Change your default terminal to `bash`
- If `~/.bashrc` is missing, then look for the file `~/.bash_profile`

Python Packages
---------------
The only Python package to install not present in Anaconda is [sortedcontainers](http://www.grantjenks.com/docs/sortedcontainers/index.html), 
and it is possible to install it by running the commands:
```bash
source activate lassim # not needed if the virtual environment doesn't exist
pip install sortedcontainers
```

MPI for Peripherals Optimization (optional)
--------------------------------
The Peripheral Genes Optimization is currently available for single node computation and multiple nodes computation on 
a cluster. In order to use it on multiple nodes, it is necessary to run the following commands, in order to install 
the necessary packages:
```bash
source activate lassim # not needed if the virtual environment doesn't exist
conda install --channel mpi4py mpich mpi4py
```