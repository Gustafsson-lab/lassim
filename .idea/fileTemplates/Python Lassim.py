#parse("user.py")

__author__ = "${USER}"
__copyright__ = "Copyright (C) 2016 ${USER}"
__license__ = "GNU General Public License v3.0"
__version__ = "${VERSION}"
