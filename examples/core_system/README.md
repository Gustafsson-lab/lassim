Core System Optimization Example
================================

Example of the Core System Optimization where a reduced network is used. The Core System
is composed of 3 transcription factors (COPEB, GATA3, ELK1) with the initial number of
supposed interactions equal to 7.

In order to run the example, the command to use is:
```
python <repository>/lassim_toolbox/source/lassim_core.py <configuration>
```

Three configuration files are available:
- `configuration.ini` is the example for optimizations using step-removal function without
the use of perturbations data.
- `configuration_l1.ini` is the example for optimizations using the L1 penalty, with edge
removal based on a given threshold.
- `configuration_perturbations.ini` is the example for optimizations using step-removal function 
with the use of perturbations data. 