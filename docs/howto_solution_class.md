HOWTO - Custom Solution Class
=============================

In order to represent solutions in a different way using **LASSIM**, it is necessary to create a custom
Core System Solution and a custom Peripheral Gene Solution. Each one of these classes must implement the
class `BaseSolution` of the `lassim` package, and overriding the necessary methods.

Below, it is possible to see the templates for the `CoreSolution` and the `PeripheralSolution` that should be
used as starting point.

```python
import pandas as pd
from lassim.base_solution import BaseSolution
from lassim.lassim_exception import LassimException
from lassim.lassim_problem import LassimProblem
from lassim.type_aliases import Vector
from sortedcontainers import SortedDict
from typing import List

class CoreSolution(BaseSolution):
    def __init__(self,
                 decision_vector: Vector,
                 fitness: Vector,
                 react_ids: SortedDict,
                 prob: LassimProblem):
        super().__init__(decision_vector, fitness, react_ids, prob)
        # user initialization
        
    def get_solution_matrix(self, headers: List[str]) -> pd.DataFrame:
        # generate a pandas DataFrame representing the solution
        
    @classmethod
    def solution_headers(cls) -> List[str]:
        # return a list of specific headers for this solution
        
class PeripheralSolution(BaseSolution):

    __gene_name = None

    def __init__(self,
                 decision_vector: Vector,
                 fitness: Vector,
                 react_ids: SortedDict,
                 prob: LassimProblem):
        super().__init__(decision_vector, fitness, react_ids, prob)
        if self.__gene_name is None:
            raise LassimException("Gene name not set!!")
        self._gene = self.__gene_name
        # user initialization
        
    def get_solution_matrix(self, headers: List[str]) -> pd.DataFrame:
        # generate a pandas DataFrame representing the solution
        
    @property
    def gene_name(self) -> str:
        return self._gene

    @classmethod
    def set_gene_name(cls, name: str):
        cls.__gene_name = name
        
    @classmethod
    def solution_headers(cls) -> List[str]:
        # return a list of specific headers for this solution
```

Examples of solution classes for LASSIM and Mass Action Kinetics can be found in `lassim_api/lassim/solutions`.