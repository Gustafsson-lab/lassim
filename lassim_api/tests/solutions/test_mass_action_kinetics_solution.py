from unittest import TestCase
from unittest.mock import create_autospec

import numpy as np
import pandas as pd
from nose.tools import assert_equal
from pandas.util.testing import assert_frame_equal
from sortedcontainers import SortedDict, SortedSet, SortedList

from lassim_api.lassim.problems.core_problem import CoreProblem
from lassim_api.lassim.problems.network_problem import NetworkProblem
from lassim_api.lassim.solutions.mass_action_kinetics_solution import \
    CoreSolution, PeripheralSolution

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


def create_fake_core_solution(cost: float) -> CoreSolution:
    decision_vector = np.array([-1, 2.3, 3, 2, -5.99])
    fitness = np.array(cost, ndmin=1)
    fake_reactions = SortedDict({
        0: SortedSet(), 1: SortedSet([0, 1, 2]), 2: SortedSet([1, 2])
    })
    mock_core = create_autospec(CoreProblem, instance=True)
    mock_core.vector_maps = (
        np.array([0, 0, 0, 1, 1, 1, 0, 1, 1], dtype=np.float),
        np.array([False, False, False, True, True, True, False, True, True])
    )
    mock_core.y0 = np.array([0, 0, 0])

    return CoreSolution(decision_vector, fitness, fake_reactions, mock_core)


class TestCoreSolution(TestCase):
    def test_GetSolutionMatrix(self):
        fake_solution = create_fake_core_solution(10.0)
        headers = CoreSolution.solution_headers() + ["TF1", "TF2", "TF3"]
        expected = pd.DataFrame(
            data=np.array([[0, 0, 0, 0], [0, -1, 2.3, 3], [0, 0, 2, -5.99]]),
            columns=headers,
            dtype=np.float
        )
        actual = fake_solution.get_solution_matrix(headers)
        assert_frame_equal(expected, actual)

    def test_SolutionsCorrectOrder(self):
        fake_solution1 = create_fake_core_solution(10.0)
        fake_solution2 = create_fake_core_solution(0.0000001)
        fake_solution3 = create_fake_core_solution(0.0000002)
        solutions = SortedList()
        solutions.add(fake_solution1)
        solutions.add(fake_solution2)
        solutions.add(fake_solution3)

        actual = solutions.pop(0)
        assert_equal(
            actual, fake_solution2,
            "Expected solution with cost {}, but received with cost {}".format(
                fake_solution2.cost, actual.cost
            ))
        actual = solutions.pop(0)
        assert_equal(
            actual, fake_solution3,
            "Expected solution with cost {}, but received with cost {}".format(
                fake_solution3.cost, actual.cost
            ))


################################################################################

def create_fake_peripheral_solution(cost: float) -> PeripheralSolution:
    decision_vector = np.array([10, 21], dtype=np.float)
    fitness = np.array(cost, ndmin=1)
    # number of transcription factors is 3
    fake_reactions = SortedDict({
        44: SortedSet([0, 2])
    })
    mock_peripheral = create_autospec(NetworkProblem, instance=True)
    mock_peripheral.vector_maps = (
        np.array([10, 15, 1, 0,
                  11, 0, 0, 0,
                  11, 1, 0, 0,
                  1, 0, 1, 0], dtype=np.float64),
        np.array([True, True, True, False,
                  True, False, False, False,
                  True, True, True, True,
                  True, False, True, False])
    )

    return PeripheralSolution(
        decision_vector, fitness, fake_reactions, mock_peripheral
    )


class TestPeripheralSolution(TestCase):
    def setUp(self):
        self.headers = PeripheralSolution.solution_headers() + \
                       ["TF1", "TF2", "TF3"]

    def tearDown(self):
        PeripheralSolution.set_gene_name(None)

    def test_SolutionMatrix(self):
        PeripheralSolution.set_gene_name("gene")
        solution = create_fake_peripheral_solution(10)
        data = np.array(["gene", 10.0, 0.0, 21.0])
        expected = pd.Series(data, self.headers).to_frame().transpose()
        actual = solution.get_solution_matrix(self.headers)
        assert_frame_equal(expected, actual)
