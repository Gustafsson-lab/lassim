__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2016 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class LassimException(Exception):
    """
    Custom Exception for the Lassim API.
    """
    pass
