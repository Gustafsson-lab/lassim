from typing import List

from sortedcontainers import SortedList

from ..solutions_handler import SolutionsHandler

__author__ = "Guido Pio Mariotti"
__copyright__ = "Copyright (C) 2017 Guido Pio Mariotti"
__license__ = "GNU General Public License v3.0"
__version__ = "0.5.0"


class CompositeSolutionsHandler(SolutionsHandler):
    """
    A SolutionsHandler that accepts one or more SolutionsHandler and dispatch
    to the the list of solutions
    """

    def __init__(self, handlers_list: List[SolutionsHandler]):
        self._handlers = handlers_list

    def handle_solutions(self, solutions: SortedList, **kwargs):
        for handler in self._handlers:
            handler.handle_solutions(solutions)
